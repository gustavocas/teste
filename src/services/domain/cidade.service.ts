import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs"
import { API_CONFIG } from "src/app/config/api.config";
import { CidadeDTO } from "src/models/cidade.dto";


@Injectable()
export class CidadeService{
    constructor (public http: HttpClient){

    }

    listaCidades(idEstado:string) : Observable<CidadeDTO[]>{
        return this.http.get<CidadeDTO[]>(`${API_CONFIG.baseURL}/cidades/?idestado=${idEstado}`);
    }

}