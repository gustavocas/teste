import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { API_CONFIG } from "src/app/config/api.config";
import { PedidoDTO } from "src/models/pedido.dto";


@Injectable()
export class PedidoService{
    
    constructor(public httpClient:HttpClient){

    }

    salvar(pedido:PedidoDTO){
        console.log(pedido);
        return this.httpClient.post(`${API_CONFIG.baseURL}/pedidos`,pedido,{
            observe: 'response',
            responseType: 'text'
        });
    }
}
