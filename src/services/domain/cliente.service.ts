import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { API_CONFIG } from "src/app/config/api.config";
import { ClienteDTO } from "src/models/cliente.dto";
import { ClienteNovoDTO } from "src/models/clienteNovo.dto";
import { StorageServicos } from "../storage.service";

@Injectable()
export class ClienteService{
    
    constructor(public httpClient:HttpClient, public storage:StorageServicos){

    }

    findByEmail(email:string) : Observable<ClienteDTO>{
        
        let usuario = this.storage.getLocalUser();
        usuario.token;//seria passado o token mas eu nao quero

        return this.httpClient.get<ClienteDTO>(`${API_CONFIG.baseURL}/clientes/email?value=${email}`);
    }

    registrar(clienteDTO:ClienteNovoDTO){
        
        return this.httpClient.post(`${API_CONFIG.baseURL}/clientes`,clienteDTO,{
            observe: 'response',
            responseType: 'text'
        });
    }

    salvar(clienteDTO:ClienteDTO){
        
        return this.httpClient.post(`${API_CONFIG.baseURL}/clientes/foto`,clienteDTO,{
            observe: 'response',
            responseType: 'text'
        });
    }
}
