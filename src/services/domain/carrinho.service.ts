import { Injectable } from "@angular/core";
import { Carrinho } from "src/models/carrinho";
import { ProdutoDTO } from "src/models/produto.dto";
import { StorageServicos } from "../storage.service";

@Injectable()
export class CarrinhoService{
    constructor (public storage : StorageServicos){

    }

    criarCarrinho():Carrinho{
        let car: Carrinho = {itens : []};
        this.storage.setCarrinho(car);
        return car;
    }    

    getCarrinho(): Carrinho{
        let car : Carrinho = this.storage.getCarrinho();
        if (car==null){
            car = this.criarCarrinho();
        }
        return car;
    }

    adicionarProduto(produto:ProdutoDTO):Carrinho{
        let car = this.getCarrinho();
        let i = car.itens.findIndex(x => x.produto.id == produto.id);
        if (i==-1){ //se o produto nao existe na lista insere
            car.itens.push({quantidade:1, produto: produto});
        }
        this.storage.setCarrinho(car);
        return car;
    }

    removerProduto(produto:ProdutoDTO):Carrinho{
        let car = this.getCarrinho();
        let i = car.itens.findIndex(x => x.produto.id == produto.id);
        if (i!=-1){ //se o produto existe na lista remove
            car.itens.splice(i, 1);
        }
        this.storage.setCarrinho(car);
        return car;
    }

    adicionarQtdProduto(produto:ProdutoDTO):Carrinho{
        let car = this.getCarrinho();
        let i = car.itens.findIndex(x => x.produto.id == produto.id);
        if (i!=-1){ //se o produto existe na lista soma
            car.itens[i].quantidade++;
        }
        this.storage.setCarrinho(car);
        return car;
    }

    removerQtdProduto(produto:ProdutoDTO):Carrinho{
        let car = this.getCarrinho();
        let i = car.itens.findIndex(x => x.produto.id == produto.id);
        if (i!=-1){ //se o produto existe na lista subtrai
            car.itens[i].quantidade--;
            if (car.itens[i].quantidade<1){
                car = this.removerProduto(car.itens[i].produto);
            }
        }
        this.storage.setCarrinho(car);
        return car;
    }

    totalCarrinho():number{
        let car = this.getCarrinho();
        let total = 0;
        for (var i=0;i<car.itens.length;i++){
            total += car.itens[i].produto.preco * car.itens[i].quantidade;
        }
        return total;
    }
}