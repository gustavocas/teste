import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs"
import { API_CONFIG } from "src/app/config/api.config";
import { ProdutoDTO } from "src/models/produto.dto";

@Injectable()
export class ProdutoService{
    constructor (public http: HttpClient){

    }

    listarProdutos(idCategoria:string, pagina:number=0) : Observable<ProdutoDTO[]>{
        return this.http.get<ProdutoDTO[]>(`${API_CONFIG.baseURL}/produtos/?categorias=${idCategoria}&pagina=${pagina}&linhasPorPagina=15`);
    }

    obterProduto(idProduto:string) : Observable<ProdutoDTO>{
        return this.http.get<ProdutoDTO>(`${API_CONFIG.baseURL}/produtos/${idProduto}`);
    }

}