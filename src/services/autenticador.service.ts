import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { API_CONFIG } from "src/app/config/api.config";
import { CredenciaisDTO } from "src/models/credenciais.dto";
import { LocalUser } from "src/models/localUser";
import { STORAGE_KEYS } from "../app/config/storage_keys";
import { StorageServicos } from "./storage.service";

@Injectable()
export class AtutenticadorService{


    constructor(public http: HttpClient, public storageService : StorageServicos){

    }

    autenticar(credencial:CredenciaisDTO){
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "http://127.0.0.1:8080/oauth/token", true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //token gerado para as credenciais:
        //username: nomedomeuaplicativo 
        //password: senhaodomeuaplicativo
        //essas credenciais são definidas na classe ServidorAutorizacao
        //pode ser gerado no site https://www.blitter.se/utils/basic-authentication-header-generator/
        xhr.setRequestHeader('Authorization', 'Basic bm9tZWRvbWV1YXBsaWNhdGl2bzpzZW5oYW9kb21ldWFwbGljYXRpdm8=');

        xhr.send("username="+credencial.email+"&password="+credencial.senha+"&grant_type=password");
        xhr.onload = function() {
            if (this.status==200){
                var data = JSON.parse(this.responseText);
                console.log(data);
                localStorage.setItem("tokenUsuario", data.token_type+" "+data.access_token);
            }else{
                console.log(this);
            }
        }
        
    }
    autenticar2(credencial:CredenciaisDTO){
        const formData = new FormData();
        formData.append('name', 'nome');
        formData.append('username', credencial.email);
        formData.append('password', credencial.senha);
        formData.append('grant_type', 'password');

        return this.http.post(`${API_CONFIG.baseURL}/oauth/token`,formData,
        {
                observe: 'response',
                responseType: 'text',
                headers: new HttpHeaders({
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Authorization': 'Basic bm9tZWRvbWV1YXBsaWNhdGl2bzpzZW5oYW9kb21ldWFwbGljYXRpdm8='
                    })
            },
        ).subscribe(
            (response) => console.log(response),
            (error) => console.log(error)
          );
    }

    loginEfetuado(res:Object){
        console.log(res);
        //this.storageService.setLocalUser(usuario);
    }

    logout(){
        this.storageService.setLocalUser(null);
    }
    
}