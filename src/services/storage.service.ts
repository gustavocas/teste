import { Injectable } from "@angular/core";
import { Carrinho } from "src/models/carrinho";
import { STORAGE_KEYS } from "../app/config/storage_keys";
import { LocalUser } from "../models/localUser";

@Injectable()
export class StorageServicos {

    getTokenUsuario() : string {
        return localStorage.getItem("tokenUsuario");
    }

    getLocalUser() : LocalUser {
        let usr = localStorage.getItem(STORAGE_KEYS.localUser);
        let usuario:LocalUser;
        if (usr == null) {
            return null;
        }
        else {
            usuario = JSON.parse(usr);
            console.log(usuario.email);
            return JSON.parse(usr);
        }
    }

    setLocalUser(obj : LocalUser) {
        if (obj == null) {
            localStorage.removeItem(STORAGE_KEYS.localUser);
        }
        else {
            localStorage.setItem(STORAGE_KEYS.localUser, JSON.stringify(obj));
        }
    }

    getCarrinho() : Carrinho {
        let key = localStorage.getItem(STORAGE_KEYS.carrinho);
        let dto:Carrinho;
        if (key == null) {
            return null;
        }
        else {
            dto = JSON.parse(key);
            return JSON.parse(key);
        }
    }

    setCarrinho(obj : Carrinho) {
        if (obj == null) {
            localStorage.removeItem(STORAGE_KEYS.carrinho);
        }
        else {
            localStorage.setItem(STORAGE_KEYS.carrinho, JSON.stringify(obj));
        }
    }

}