import { Injectable } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from 'rxjs/operators';
import { StorageServicos } from "src/services/storage.service";
 
 
@Injectable()
export class CapturaRequisicao implements HttpInterceptor{
 
    constructor(public storageService:StorageServicos){

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        console.log("INTERCEPTOU REQUISICAO");

        let tokenUsuario = this.storageService.getTokenUsuario();
        if(tokenUsuario){//se existe usuario logado
            //clona a requisicao original adicionando o cabecalho com o token:
            const requisicao = req.clone({headers:req.headers.set("Authorization",tokenUsuario)})
            return next.handle(requisicao);
        }else{
            return next.handle(req);
        }
                
    }
 
 
}
 
 
export const CapturaRequisicaoProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: CapturaRequisicao,
    multi: true,
};