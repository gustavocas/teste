import { CarrinhoItem } from "./carrinho.item";
import { ClienteDTO } from "./cliente.dto";
import { EnderecoDTO } from "./endereco.dto";
import { PagamentoDTO } from "./pagamento.dto";

export interface PedidoDTO{
	id:string;
    cliente:Object;
	enderecoEntrega:EnderecoDTO;
	pagamento:PagamentoDTO;
	itens:CarrinhoItem[];
}