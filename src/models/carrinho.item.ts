import { ProdutoDTO } from "./produto.dto";

export interface CarrinhoItem{
    quantidade:number;
    produto:ProdutoDTO;
}