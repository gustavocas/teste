import { CarrinhoItem } from "./carrinho.item";

export interface Carrinho{
    itens:CarrinhoItem[];
}