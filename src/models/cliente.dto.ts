import { EnderecoDTO } from "./endereco.dto";

export interface ClienteDTO{
    id:string;
    nome:string;
    email:string;
    senha:string;
    foto:string;
    documento:string;
    enderecos:EnderecoDTO[];
}