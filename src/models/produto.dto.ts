export interface ProdutoDTO{
    id:string;
    nome:string;
    preco:number;
    foto:string;
}