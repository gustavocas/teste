export interface ClienteNovoDTO{
    nome:string;
	email:string;
	
	documento:string;
	tipo:string;

	logradouro:string;
	numero:string;
	complemento:string;
	bairro:string;
	cep:string;
	
    idCidade:string;

	telefone1:string;
	telefone2:string;
	telefone3:string;
	senha:string;
}