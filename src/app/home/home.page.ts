import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CredenciaisDTO } from 'src/models/credenciais.dto';
import { LocalUser } from 'src/models/localUser';
import { AtutenticadorService } from 'src/services/autenticador.service';
import { DataService, Message } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private router: Router, private data: DataService, public autenticador: AtutenticadorService) {}

  credenciais : CredenciaisDTO = {
    email: "",
    senha: "",
    token: ""
  }

  refresh(ev) {
    setTimeout(() => {
      ev.detail.complete();
    }, 3000);
  }

  getMessages(): Message[] {
    return this.data.getMessages();
  }

  login(){
        
    this.autenticador.autenticar(this.credenciais);
    /*.subscribe(
      response => {
        this.autenticador.loginEfetuado(response);
        //this.router.navigate(['/perfil']);
      },error => { //se o erro deve ser capturado pelo capturaErro.ts
        console.log("ERRO:");
        console.log(error)}//se deu errro tb escreve ue
     )*/
    
  }
  

}
