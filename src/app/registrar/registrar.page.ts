import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { CidadeDTO } from 'src/models/cidade.dto';
import { ClienteNovoDTO } from 'src/models/clienteNovo.dto';
import { EstadoDTO } from 'src/models/estado.dto';
import { CidadeService } from 'src/services/domain/cidade.service';
import { ClienteService } from 'src/services/domain/cliente.service';
import { EstadoService } from 'src/services/domain/estado.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.page.html',
  styleUrls: ['./registrar.page.scss'],
})
export class RegistrarPage implements OnInit {



  constructor(
    public clienteServico:ClienteService, 
    public cidadeService:CidadeService, 
    public estadoService:EstadoService,
    public alertCont:AlertController,
    private router: Router) { 

  }

  listaEstados: EstadoDTO[];
  listaCidades: CidadeDTO[];

  clienteDTO:ClienteNovoDTO= {
    nome:"",
    email:"",

    documento:"",
    tipo:"1",

    logradouro:"",
    numero:"",
    complemento:"",
    bairro:"",
    cep:"",

    idCidade:"",

    telefone1:"",
    telefone2:"",
    telefone3:"",
    senha:""
  }

  ngOnInit() {
    this.estadoService.listaEstados().subscribe(
      response => {
        this.listaEstados = response;
        //this.buscarCidades();
        console.log("deucerto estado");
        console.log(response)}//se deu certo escreve no log
      ,error => { //se o erro deve ser capturado pelo capturaErro.ts
       console.log("ERRO:");
       console.log(error)}//se deu errro tb escreve ue
    )
  }

  buscarCidades(est:any){
    this.cidadeService.listaCidades(est).subscribe(
      response => {
        this.listaCidades = response;
        console.log("deucerto cidade");
        console.log(response)}//se deu certo escreve no log
      ,error => { //se o erro deve ser capturado pelo capturaErro.ts
       console.log("ERRO:");
       console.log(error)}//se deu errro tb escreve ue
    )
  }

  registrarUsuario(){
    console.log(this.clienteDTO);
    this.clienteServico.registrar(this.clienteDTO).subscribe(
      response => {        
        this.presentAlert("Cliente Salvo","Cliente Salvo");
      },error => { //se o erro deve ser capturado pelo capturaErro.ts
        console.log("ERRO:");
        console.log(error)}//se deu errro tb escreve ue
     )
  }

  presentAlert(titulo:string, mensagem: string) {
    const alert = this.alertCont.create({
    message: mensagem,
    subHeader: titulo,
    buttons: [{
      text: 'OK',
      handler: () =>{
        this.router.navigate(['/home']);//qdo aperta OK volta para o home
      }
    }]
  }).then(alert=> alert.present());
}

}
