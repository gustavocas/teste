import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavParams } from '@ionic/angular';
import { PedidoDTO } from 'src/models/pedido.dto';
import { CarrinhoService } from 'src/services/domain/carrinho.service';

@Component({
  selector: 'app-detalhes-pedido',
  templateUrl: './detalhes-pedido.page.html',
  styleUrls: ['./detalhes-pedido.page.scss'],
})
export class DetalhesPedidoPage implements OnInit {

  pedidoDTO:PedidoDTO;
  constructor(private router: Router,private activatedRoute: ActivatedRoute, public carrinhoService:CarrinhoService) {

    this.activatedRoute.params.subscribe((params) => {
      this.pedidoDTO=JSON.parse(params.pedido);
      console.log(this.pedidoDTO);
    });
    

   }

  ngOnInit() {
  }

  totalCarrinho() : number{
    return this.carrinhoService.totalCarrinho();
  }

}
