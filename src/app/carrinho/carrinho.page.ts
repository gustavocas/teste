import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { CarrinhoItem } from 'src/models/carrinho.item';
import { ProdutoDTO } from 'src/models/produto.dto';
import { CarrinhoService } from 'src/services/domain/carrinho.service';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.page.html',
  styleUrls: ['./carrinho.page.scss'],
})
export class CarrinhoPage implements OnInit {

  listaItens: CarrinhoItem [];

  constructor(
    public menu: MenuController,
    public carrinhoService:CarrinhoService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.listaItens = this.carrinhoService.getCarrinho().itens;
  }

  removerProduto(produto:ProdutoDTO){
    this.listaItens = this.carrinhoService.removerProduto(produto).itens;
  }

  adicionarQtdProduto(produto:ProdutoDTO){
    this.listaItens = this.carrinhoService.adicionarQtdProduto(produto).itens;
  }

  removerQtdProduto(produto:ProdutoDTO){
    this.listaItens = this.carrinhoService.removerQtdProduto(produto).itens;
  }

  totalCarrinho() : number{
    return this.carrinhoService.totalCarrinho();
  }

}
