import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProdutoDTO } from 'src/models/produto.dto';
import { CarrinhoService } from 'src/services/domain/carrinho.service';
import { ProdutoService } from 'src/services/domain/produto.service';

@Component({
  selector: 'app-produto-detalhe',
  templateUrl: './produto-detalhe.page.html',
  styleUrls: ['./produto-detalhe.page.scss'],
})
export class ProdutoDetalhePage implements OnInit {

  produtoDTO : ProdutoDTO;

  constructor(
    public produtoServico:ProdutoService,
    private route: ActivatedRoute,
    public carrinhoService:CarrinhoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams
       .subscribe(params => {
        console.log(params.categoria); 
        this.obterProduto(params.produto);
      }
    );
  }

  obterProduto(idProduto){
    this.produtoServico.obterProduto(idProduto).subscribe(
      response => {
        this.produtoDTO = response;
      }
      ,error => {
       console.log("ERRO:");
       console.log(error);
      }
    )  
  }

  adicionarCarrinho(produtoDTO : ProdutoDTO){
    this.carrinhoService.adicionarProduto(produtoDTO);
    this.router.navigate(['/carrinho']);
  }

}
