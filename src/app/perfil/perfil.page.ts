import { Component, OnInit } from '@angular/core';
import { Navigation, Router } from '@angular/router';
import { ClienteDTO } from 'src/models/cliente.dto';
import { ClienteService } from 'src/services/domain/cliente.service';
import { StorageServicos } from 'src/services/storage.service';
import { MenuComponent } from '../menu/menu.component';
import * as _ from 'lodash';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  constructor(
    private router: Router,
    public menu: MenuComponent,
    public storage: StorageServicos,
    public alertCont: AlertController,
    public clienteService: ClienteService
  ) { }

  clienteDTO: ClienteDTO;
  isImageSaved: boolean;
  cardImageBase64: string;

  ngOnInit() {
    this.menu.openCustom();
    let usuario = this.storage.getLocalUser();
    console.log("oi:" + usuario.email);
    if (usuario && usuario.email) {
      console.log("usuusu: " + usuario);
      this.clienteService.findByEmail(usuario.email).subscribe(
        response => {
          this.clienteDTO = response;
          if (this.clienteDTO.foto != null) {
            this.cardImageBase64 = this.clienteDTO.foto;
            this.isImageSaved = true;
          }
          console.log("deucerto");
          console.log(response)
        }//se deu certo escreve no log
        , error => { //se o erro deve ser capturado pelo capturaErro.ts
          console.log("ERRO:");
          console.log(error)
          this.router.navigate(['/home']);
        }

      )
    } else {
      this.router.navigate(['/']);//se usuario nao estiver definido volta para o home
    }
  }

  salvarClienteFoto() {
    console.log(this.clienteDTO);
    this.clienteService.salvar(this.clienteDTO).subscribe(
      response => {
        console.log("FOTO salva");
      }, error => { //se o erro deve ser capturado pelo capturaErro.ts
        console.log("ERRO:");
        console.log(error)
      }//se deu errro tb escreve ue
    )
  }



  salvarFoto(fileInput: any) {

    if (fileInput.target.firstElementChild.files && fileInput.target.firstElementChild.files[0]) {
      // Size Filter Bytes
      const max_size = 1500000;
      const allowed_types = ['image/png', 'image/jpeg'];
      const max_height = 15200;
      const max_width = 25600;

      if (fileInput.target.firstElementChild.files[0].size > max_size) {
        this.presentAlert('Imagem muito grande','Tamanho permitido ate ' + max_size / 1000 + 'Mb');
        return false;
      }

      if (!_.includes(allowed_types, fileInput.target.firstElementChild.files[0].type)) {
        this.presentAlert('Formato invalido','Selecione uma imagem no formato JPG ou PNG');
        return false;
      }
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];

          console.log(img_height, img_width);


          if (img_height > max_height && img_width > max_width) {
            this.presentAlert('Imagem muito grande','Dimensoes permitidas ' +
            max_height +
            '*' +
            max_width +
            'px');
            return false;
          } else {
            let imgBase64Path = e.target.result;
            this.cardImageBase64 = imgBase64Path;
            this.clienteDTO.foto = imgBase64Path;
            this.isImageSaved = true;
            this.salvarClienteFoto();
            // this.previewImagePath = imgBase64Path;
          }
        };
      };

      reader.readAsDataURL(fileInput.target.firstElementChild.files[0]);
    }
  }

  presentAlert(titulo: string, mensagem: string) {
    const alert = this.alertCont.create({
      message: mensagem,
      subHeader: titulo,
      buttons: [{
        text: 'OK',
        handler: () => {
          this.router.navigate(['/home']);//qdo aperta OK volta para o home
        }
      }]
    }).then(alert => alert.present());
  }

}
