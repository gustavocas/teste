import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, MenuController } from '@ionic/angular';
import { ProdutoDTO } from 'src/models/produto.dto';
import { ProdutoService } from 'src/services/domain/produto.service';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.page.html',
  styleUrls: ['./produtos.page.scss'],
})
export class ProdutosPage implements OnInit {

  listaProduto: ProdutoDTO []=[];
  idCategoria:string="0";
  pagina:number=0;

  constructor(
    public menu: MenuController,
    public produtoServico:ProdutoService,
    private route: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.route.queryParams
       .subscribe(params => {
        this.idCategoria=params.categoria; 
        this.listarProtutos();
      }
    );
  }

  listarProtutos(){
    this.mostrarLoading();
    
    this.produtoServico.listarProdutos(this.idCategoria,this.pagina).subscribe(
      response => {
        this.listaProduto = this.listaProduto.concat(response['content']);//o atributo content eh pra pegar os produtos paginados
        this.esconderLoading();
      }
      ,error => {
       console.log("ERRO:");
       console.log(error);
       this.esconderLoading();
      }
    )
  }

  showProdutoDetalhe(codProduto : string){
    this.router.navigate(['/produto-detalhe'],{ queryParams: { produto: codProduto } });
  }

  async mostrarLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Aguardem...',
      duration: 1000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  /**
   * Dismiss all the pending loaders, if any
   */
  async esconderLoading() {
    while (await this.loadingController.getTop() !== undefined) {
      await this.loadingController.dismiss();
    }
  }

  //esse metodo eh chamado qdo exibe o compomente infinity. ele vai exibir a animacao por 500ms, incrementar o numero da pagina e chamar o metodo q carrega os produtos
  loadData(event) {
    this.pagina++;
    this.listarProtutos();
    setTimeout(() => {
      event.target.complete();

    }, 500);
  }

}
