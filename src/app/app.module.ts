import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { CategoriaService } from 'src/services/domain/categoria.service';
import { ErrorInterceptorProvider } from 'src/capturadorErro/capturaErro';
import { AtutenticadorService } from 'src/services/autenticador.service';
import { StorageServicos } from 'src/services/storage.service';
import { MenuComponent } from './menu/menu.component';
import { ClienteService } from 'src/services/domain/cliente.service';
import { CapturaRequisicaoProvider } from 'src/capturadorRequisicao/capturaRequisicao';
import { EstadoService } from 'src/services/domain/estado.service';
import { CidadeService } from 'src/services/domain/cidade.service';
import { ProdutoService } from 'src/services/domain/produto.service';
import { CarrinhoService } from 'src/services/domain/carrinho.service';
import { PedidoService } from 'src/services/domain/pedido.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, HttpClientModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, 
    CategoriaService,
    CapturaRequisicaoProvider,//tem q ser ante do interceptador de erro, pois tem q executar antes 
    ErrorInterceptorProvider,
    AtutenticadorService,
    StorageServicos,
    MenuComponent, 
    ClienteService,
    EstadoService,
    CidadeService,
    ProdutoService,
    CarrinhoService,
    PedidoService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
