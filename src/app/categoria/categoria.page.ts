import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { CategoriaDTO } from 'src/models/categoria.dto';
import { CategoriaService } from 'src/services/domain/categoria.service';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.page.html',
  styleUrls: ['./categoria.page.scss'],
})
export class CategoriaPage implements OnInit {

  listaCategoria: CategoriaDTO[];

  constructor(
    public menu: MenuController,
    public categoriaService: CategoriaService,
    private router: Router
    ) { }

  ionViewWillEnter(){
    console.log("entrou.. UII!!!!");
  }

  ngOnInit() {
    this.categoriaService.findAll().subscribe(
      response => {
        this.listaCategoria = response;
        console.log("deucerto");
        console.log(response)}//se deu certo escreve no log
      ,error => { //se o erro deve ser capturado pelo capturaErro.ts
       console.log("ERRO:");
       console.log(error)}//se deu errro tb escreve ue
    )
  }

  showProdutos(codCategoria : string){
    console.log("CATEFORORORO: "+codCategoria);
    this.router.navigate(['/produtos'],{ queryParams: { categoria: codCategoria } });
  }

}
