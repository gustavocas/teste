import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ClienteDTO } from 'src/models/cliente.dto';
import { EnderecoDTO } from 'src/models/endereco.dto';
import { PagamentoDTO } from 'src/models/pagamento.dto';
import { PedidoDTO } from 'src/models/pedido.dto';
import { CarrinhoService } from 'src/services/domain/carrinho.service';
import { ClienteService } from 'src/services/domain/cliente.service';
import { PedidoService } from 'src/services/domain/pedido.service';
import { StorageServicos } from 'src/services/storage.service';

@Component({
  selector: 'app-escolher-endereco',
  templateUrl: './escolher-endereco.page.html',
  styleUrls: ['./escolher-endereco.page.scss'],
})
export class EscolherEnderecoPage implements OnInit {

  enderecos: EnderecoDTO[];
  pedidoDTO:PedidoDTO;
  clienteDTO:ClienteDTO;
  enderecoSelecionado:EnderecoDTO;
  pagamentoDTO:PagamentoDTO;

  constructor(
    private router: Router,
    public storage: StorageServicos,
    public clienteService:ClienteService, 
    public alertController: AlertController, 
    public pedidoService:PedidoService,
    public carrinhoService:CarrinhoService
    ) { }

  ngOnInit() {
    this.enderecos = this.obterEnderecosCliente();
  }

  obterEnderecosCliente():EnderecoDTO[]{
    let listaEnderecos : EnderecoDTO[];

    let usuario = this.storage.getLocalUser();
    if (usuario && usuario.email){
      this.clienteService.findByEmail(usuario.email).subscribe(
        response => {
          this.clienteDTO = response;
          this.enderecos = this.clienteDTO.enderecos;
        }
        ,error => { //se o erro deve ser capturado pelo capturaErro.ts
         this.exibeAlerta("Erro de autenticacao","Efetue o login para continuar");
         this.router.navigate(['/home']);
        }
         
      )
    }else{
      this.exibeAlerta("Usuario nao logado","Efetue o login para continuar");
      this.router.navigate(['/']);//se usuario nao estiver definido volta para o home
    }

    return listaEnderecos;
  }

  exibeAlerta(titulo:string, mensagem: string) {
    const alert = this.alertController.create({
    message: mensagem,
    subHeader: titulo,
    buttons: ['OK']}).then(alert=> alert.present());
  }

  setEndereco(endereco:EnderecoDTO){
    this.enderecoSelecionado = endereco;
  }

  setPagamento(tipo:Number){
    let pagamento:PagamentoDTO;
    if (tipo==1){
      pagamento={
        numeroParcelas : 1,
        "@type": "PagamentoBoleto"
      }
    }else{
      pagamento={
        numeroParcelas : 10,
        "@type": "PagamentoCartao"
      }
    }
    

    this.pagamentoDTO = pagamento;
  }

  salvarPedido(){
    

    let carrinho = this.carrinhoService.getCarrinho();

    this.pedidoDTO={
      id:null,
      cliente :{
        id:this.clienteDTO.id
      },
      enderecoEntrega : this.enderecoSelecionado,
      pagamento : this.pagamentoDTO,
      itens : carrinho.itens
      }
    
    this.pedidoService.salvar(this.pedidoDTO).subscribe(
      response => { 
        let loc = response.headers.get("location").split("/");
        let idPedido = loc[loc.length-1]
        this.pedidoDTO.id=idPedido;
        console.log();
        this.exibeAlerta("Pedido Salvo","o codigo do seu novo pedido: " + idPedido);
        this.carrinhoService.criarCarrinho();
        this.router.navigate(['/detalhes-pedido',{pedido:JSON.stringify(this.pedidoDTO)}]);

        //let ped = this.pedidoDTO;
        //this.router.navigate(['/detalhes-pedido',{pedido:ped}]);
      },error => { //se o erro deve ser capturado pelo capturaErro.ts
        console.log("ERRO:");
        console.log(error)}//se deu errro tb escreve ue
     )   
    
  }
  

}
