import { Injectable } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from 'rxjs/operators';
import { StorageServicos } from "src/services/storage.service";
import { AlertController } from '@ionic/angular';
import { CampoValor } from "src/models/campovalor";
 
 
@Injectable()
export class ErrorInterceptor implements HttpInterceptor{
 
    constructor(public storageService: StorageServicos, public alertController: AlertController){

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        return next.handle(req)
                .pipe(
                    catchError(error => {
                        console.log("CATOU O ERRO NO INTERCEPTADOR!!!");
                        //td essa papagaiada, eh pra pegar o erro dentro do erro
                        let errorObj = error;
                        if (errorObj.error) {
                            errorObj = errorObj.error;
                        }
                        if (!errorObj.status) {
                            errorObj = JSON.parse(errorObj);
                        }
            
                        console.log("Erro detectado pelo interceptor:");
                        console.log(errorObj);

                        
                        
                        switch(errorObj.status){
                            case 403:
                                this.presentAlert("Acesso negado",errorObj.mensagem);
                                this.storageService.setLocalUser(null);//se der erro de acesso limpa usuario
                            break;

                            case 422:
                                let campos = this.obterCamposErro(errorObj.erros);                                
                                this.presentAlert(errorObj.mensagem,campos);
                            break;

                            default:
                                this.presentAlert(errorObj.error,errorObj.mensagem);
                        }
            
                        return Observable.throw(errorObj);
                    })) as any;
    }

    presentAlert(titulo:string, mensagem: string) {
        const alert = this.alertController.create({
        message: mensagem,
        subHeader: titulo,
        buttons: ['OK']}).then(alert=> alert.present());
    }

    obterCamposErro(erros:CampoValor[]): string{
        let s : string="";
        for (var i=0;i<erros.length;i++){
            s = s + erros[i].campo + ": " + erros[i].valor+"<br>";
            console.log("numero de erros: "+erros[i].valor);
        
        }
        return s;
    }
 
}
 
 
export const ErrorInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true,
};